public abstract class Wall {
    public abstract float f(float x);
    public boolean isAbove(float x, float y) {
        return (y < f(x));
    }
}