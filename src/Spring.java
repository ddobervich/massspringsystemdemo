import processing.core.PApplet;
import processing.core.PVector;

public class Spring {
    private Mass m1, m2;
    private float k;        // spring stiffness constant
    private float restingLength;

    private int color;

    public Spring(Mass m1, Mass m2, float k) {
        this(m1, m2, k, m1.distanceTo(m2)*0.9f);
    }

    public Spring(Mass m1, Mass m2, float k, float restingLength) {
        this.m1 = m1;
        this.m2 = m2;
        this.k = k;
        this.restingLength = restingLength;
    }

    public float getLengthDisplacement() {
        return (m1.distanceTo(m2) - restingLength);
    }

    public void applyForceToMasses() {
        PVector f = PVector.sub(m1.getPosition(), m2.getPosition());
        f.normalize();
        f.mult( getLengthDisplacement()*(-k) );
        m1.addForce(f);
        m2.addForce(PVector.mult(f,-1));

        // Apply damping force.
        PVector f2 = PVector.sub(m2.getVelocity(), m1.getVelocity());
        PVector L = PVector.sub(m1.getPosition(), m2.getPosition());

        float val = f2.dot(L)/L.mag();
        L.normalize();
        L.mult(val);

        float d = 0.01f;
        L.mult(d);
        m1.addForce(L);
        m2.addForce(PVector.mult(L, -1));
    }

    public void draw(PApplet window) {
        window.line(m1.getPosition().x, m1.getPosition().y, m2.getPosition().x, m2.getPosition().y);
    }
}
