import processing.core.PApplet;
import processing.core.PVector;

import java.util.ArrayList;

public class MassSpringSystem {
    private ArrayList<Mass> masses;
    private ArrayList<Spring> springs;
    private ArrayList<Wall> walls;
    private ArrayList<Spring> deadList = new ArrayList<>();

    public MassSpringSystem() {
        masses = new ArrayList<>();
        springs = new ArrayList<>();
        walls = new ArrayList<>();
    }

    public void addMass(Mass m) {
        masses.add(m);
        m.registerWithSystem(this);
    }

    public void addSpring(Spring s) {
        springs.add(s);
    }

    /*
    Update forces, accel, vel and positions of everything
     */
    public void update() {

        for (Spring s : springs) {
            s.applyForceToMasses();
        }

        for (int i = 0; i < masses.size(); i++) {
            masses.get(i).update(walls);
        }
    }

    public void draw(PApplet window) {
        drawMasses(window);
        drawSprings(window);
    }

    private void drawSprings(PApplet window) {
        for (Spring s : springs) {
            s.draw(window);
        }
    }

    private void drawMasses(PApplet window) {
        for (Mass m: masses) {
            m.draw(window);
        }
    }

    public void detectCollisionsWith(Wall w) {
        this.walls.add(w);
    }

    public void applyForceToAll(int x, float y) {
        for (Mass m : masses){
            m.addForce(x, y);
        }
    }
}