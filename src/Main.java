import processing.core.PApplet;
import processing.core.PVector;

public class Main extends PApplet {
    MassSpringSystem bob;
    Wall w;

    public void setup() {
        size(600, 600);

        w = new Wall() {
            @Override
            public float f(float x) {
                return 450;
            }
        };

        bob = new MassSpringSystem();
        //   bob.detectCollisionsWith(w);
        Mass m1 = new Mass(1, 20, 200, 300);
     /*   Mass m2 = new Mass(1, 20, 400, 300);
        Mass m3 = new Mass(1, 20, 0, 0);*/
        m1.setRandomPosition(0, 600);
  /*      m2.setRandomPosition(0, 600);
        m3.setRandomPosition(0, 600);

        Spring s1 = new Spring(m1, m2, 0.01f, 150);
        Spring s2 = new Spring(m1, m3, 0.01f, 150);
        Spring s3 = new Spring(m2, m3, 0.01f, 150);*/

        bob.addMass(m1);
  /*      bob.addMass(m2);
        bob.addMass(m3);
        bob.addSpring(s1);
        bob.addSpring(s2);
        bob.addSpring(s3);*/
    }

    public void draw() {
        background(255);        // wipe screen with white
       // line(0, 450, width, 450);

        bob.applyForceToAll(0, 0.1f);
        bob.update();
        bob.draw(this);
    }

    public static void main(String[] args) {
        PApplet.main("Main");
    }
}
