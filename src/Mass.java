import processing.core.PApplet;
import processing.core.PVector;

import java.util.ArrayList;

public class Mass {
    private static final int DEFAULT_COLOR = 0; // black
    private boolean movable = true;
    private PVector pos, vel, acc;
    private float mass;
    private int color;
    private float radius;
    MassSpringSystem system;

    public Mass(float mass, int color, float radius, float x, float y) {
        this.pos = new PVector(x, y);
        this.mass = mass;
        this.color = color;
        this.radius = radius;
        this.vel = new PVector(0, 0);
        this.acc = new PVector(0, 0);
    }

    public Mass(float mass, float radius, float x, float y) {
        this(mass, DEFAULT_COLOR, radius, x, y);
    }

    public Mass(int mass, int radius, float x, float y, boolean movable) {
        this(mass, radius, x, y);
        this.movable = movable;
    }

    public void addForce(PVector force) {
        force.div(this.mass);
        acc.add(force);
    }

    public void addForce(float x, float y) {
        if (!movable) return;

        PVector f = new PVector(x, y);
        this.addForce(f);
    }

    public void update(ArrayList<Wall> walls) {
        if (!movable) return;

        vel.add(acc);
        acc.set(0, 0);  // reset acceleration

        PVector updatedPos = PVector.add(pos, vel);

        // check for collisions
        for (Wall w : walls) {
            // check if we cross the boundry
            if (w.isAbove(pos.x, pos.y) != w.isAbove(updatedPos.x, updatedPos.y)) {
                MathLib.Point collisionPoint = findCollisionPoint(w, pos, updatedPos);
                PVector colPos = new PVector(collisionPoint.x, collisionPoint.y) ;

                PVector v1 = PVector.sub(updatedPos, colPos);
                // reflect v1 around wall
                // updatedPos = colPos + reflected_v1
                // vel = reflected_v1*vel.mag()*0.9
            }
        }

        this.pos = updatedPos;
    }

    private MathLib.Point findCollisionPoint(Wall w, PVector pos, PVector updatedPos) {
        MathLib.Point wall1 = new MathLib.Point(0, w.f(0));
        MathLib.Point wall2 = new MathLib.Point(600, w.f(600));
        MathLib.Point path1 = new MathLib.Point(pos.x, pos.y);
        MathLib.Point path2 = new MathLib.Point(updatedPos.x, updatedPos.y);
        return MathLib.lineLineIntersection(wall1, wall2, path1, path2);
    }

    public void draw(PApplet window) {
        window.fill(color);
        window.ellipse(pos.x, pos.y, radius * 2, radius * 2);
    }

    public PVector getPosition() {
        return pos;
    }

    public float distanceTo(Mass other) {
        return this.pos.dist(other.pos);
    }

    public void setRandomPosition(int min, int max) {
        int x = (int) (Math.random() * (max - min + 1));
        int y = (int) (Math.random() * (max - min + 1));
        this.pos.set(x, y);
    }

    public PVector getVelocity() {
        return this.vel;
    }

    public void registerWithSystem(MassSpringSystem sys) {
        this.system = sys;
    }
}